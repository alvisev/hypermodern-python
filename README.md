# README

![banner](extras/banner.jpg)

A little exploration of modern python development best-practices!

I will be going through this [excellent tutorial](https://cjolowicz.github.io/posts/hypermodern-python-01-setup/) and take notes along the way on new tools and workflows to integrate in my day-to-day python development routine. 

📚 Check the [wiki](https://gitlab.com/alvisev/hypermodern-python/-/wikis/home) for updates!
