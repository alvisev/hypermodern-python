import locale
import textwrap

import click
import requests

from . import __version__

def compose_API(lang: str) -> str:
    """Return the url to the Wikipedia ``random/summary`` API, for the given
    language edition.

    Args:
        lang:   language of requested API

    Returns:
        url of the Wikipedia ``random/summary`` API for the given language.
    """
    return f"https://{lang}.wikipedia.org/api/rest_v1/page/random/summary"

@click.command()
@click.version_option(version=__version__)
@click.option('-l', '--lang', 'language',
              default=locale.getlocale()[0].split('_')[0],
              show_default=True,
              help="Set preferred language edition.")
def main(language):
    """The hypermodern Python project."""

    API_URL = compose_API(lang=language)

    with requests.get(API_URL) as response:

        try:
            response.raise_for_status()

            data = response.json()

            title = data["title"]
            extract = data["extract"]

            click.secho(title, fg="green")
            click.echo(textwrap.fill(extract))

        except requests.exceptions.HTTPError as e:
            click.secho(f"Ooops, something went wrong... the API is not "
                        f"reachable!")
